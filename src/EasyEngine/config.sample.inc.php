<?php

/**   Archivo de Ejemplo para la configuración de conexión a la base de datos
  *   @author Federicorp <federicorp@easypanelphp.com>
  *   @copyright 2015
  *   @version 2.0
  *   @package EasyEnginePHP
  *
  *   Antes de tocar este archivo, asegurece de saber los conceptos de Base de Datos y su conexión.
  *   Edite este archivo y luego guarde como "config.php" 
  *   
  */

  /** En las lineas siguientes podrá modificar las configuraciones de conexión. 
   *  Debe modificar siempre la palabra situada luego del simbolo ( => )
   *  Al modificar la palabra antes del simbolo ( => ) puede enfrentar errores en el software.
   *  Ejemplos:
   *    "HOST"  =>    "miservidor.com"           <--- Correcto
   *    "miservidor.com"  =>    "miservidor.com"    <--- Incorrecto
   */
   
   return array(		
		
		//<----------------  CONFIGURACIONES DE SEGURIDAD ------------------>//
		"ENC_SALT" => "put-here-a-random-string",
		
        
        //<------------  Configuraciones de Base de datos  ------------------>//
        
        //------------  Usar Base de Datos  ------------------//
        "USE_BD" =>   FALSE    ,
        
        "BD" =>  array(
            
            //------------  Configuracion de primera BD  ------------------//
            array(
                
                //------  Tipo de conexion  ------//
                "TYPE" =>    "MYSQL"    ,
                
                //------  Servidor de BD  ------//
                "HOST" =>    "localhost"    , //En algunos casos no hace falta tocar esta línea
                
                //------  Nombre de BD  ------//
                "NAMEDB" =>    ""    ,
                
                //------  Usuario de BD  ------//
                "USER" =>    "root"    ,
                
                //------  Pass de Usuario de BD  ------//
                "PASS" =>    ""
                
            ),
            
            //------------  Configuracion de segunda BD  ------------------//
            array(
            
            )
        
        )
    
    );
    