<?php

namespace EasyEngine\GUI\Controllers;

use \EasyEngine\Classes\View;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Installer extends BaseController
{
    public function index()
    {
        
        if (!file_exists(ABS_PATH . "config.inc.php")) {

            $view = new View("firstStep", __DIR__ . '/../Views/Installer/');

            $view->setVar("ENC_SALT", sha1(time() . uniqid()));
        } else {
            $view = new View("installed", __DIR__ . '/../Views/Installer/');
        }
        
        return $view;
    }
    
    public function install()
    {
               
        if (!file_exists(ABS_PATH . "config.inc.php")) {
            $ENC_TYPE = isset($_POST['ENC_SALT']) ? $_POST['ENC_SALT'] : "MYSQL";
            $USE_BD = isset($_POST['USE_BD']) && $_POST['USE_BD'] == "on" ? true : false;
            $BDTYPE = isset($_POST['BD_TYPE']) ? $_POST['BD_TYPE'] : "MYSQL";
            $BDHOST = isset($_POST['BD_HOST']) ? $_POST['BD_HOST'] : "localhost";
            $BDNAMEDB = isset($_POST['BD_NAMEDB']) ? $_POST['BD_NAMEDB'] : "";
            $BDUSER = isset($_POST['BD_USER']) ? $_POST['BD_USER'] : "";
            $BDPASS = isset($_POST['BD_PASS']) ? $_POST['BD_PASS'] : "";

            if ($USE_BD) {
                $link = new \mysqli($BDHOST, $BDUSER, $BDPASS, $BDNAMEDB);
                if (mysqli_connect_errno()) {
                    printf("Conexión fallida: %s\n", mysqli_connect_error());
                    exit();   
                }

                // Se conecto correctamente

                // Se obtiene el dump
                ob_start();
                include_once(__DIR__ . "/../../easyenginedb.sql");
                $query = ob_get_clean();
                ob_end_clean();

                if ($link->multi_query($query)) {
                    $finished = false;
                    do {

                        if ($result = $link->store_result()) {


                            $result->free();

                        }

                        if (!$link->more_results()) {
                            $finished = true;
                        }

                    } while (!$finished && $link->next_result());
                }
            }

            if (file_exists(ABS_PATH . "config.sample.inc.php") || copy(__DIR__ . "/../../config.sample.inc.php", ABS_PATH . "config.sample.inc.php")) {

                if (!file_exists(ABS_PATH . "config.inc.php")) {
                    
                    $confs = $this->app->getCfg();
                    $confs['ENC_SALT'] = $ENC_TYPE;
                    $confs['USE_BD'] = $USE_BD;
                    $confs['BD'][0]['TYPE'] = $BDTYPE;
                    $confs['BD'][0]['HOST'] = $BDHOST;
                    $confs['BD'][0]['NAMEDB'] = $BDNAMEDB;
                    $confs['BD'][0]['USER'] = $BDUSER;
                    $confs['BD'][0]['PASS'] = $BDPASS;
                    
                    if (!file_put_contents(ABS_PATH . "config.inc.php", "<?php \n\n return " . var_export($confs, true) . ";")) {
                        $view = new View("error", __DIR__ . '/../Views/Installer/');
                    }
                }

                $view = new View("success", __DIR__ . '/../Views/Installer/');

            } else {

                $view = new View("error", __DIR__ . '/../Views/Installer/');
            }
        } else {
            $view = new View("installed", __DIR__ . '/../Views/Installer/');
        }

        return $view;
    }
}