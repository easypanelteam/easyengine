<?php

namespace EasyEngine\GUI\Controllers;

use EasyEngine\Classes\Page;
use EasyEngine\Classes\Controller;

class BaseController extends Controller
{

    
    public function __construct()
    {
        parent::__construct();
    }
}