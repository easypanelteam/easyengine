<?php

namespace EasyEngine\Classes;

class Updater
{

    /**
     * @var App
     */
    private $app;

    public function __construct()
    {
        $this->app = App::getInstance();
    }

    /**
     * Funcion para obtener la version de easyengine
     *
     * @return bool|string
     */
    public function getCurrentVersion()
    {

        $versionReturn = false;

        $composerLockFile = ABS_PATH . "/composer.lock";
        if (file_exists($composerLockFile)) {

            $composerJson = file_get_contents($composerLockFile);
            $composer = json_decode($composerJson, true);

            foreach ($composer['packages'] as $repository) {

                if ($repository['name'] == "easypanelteam/easyengine") {
                    $versionReturn = $repository['version'] == 'dev-master' ? 'master' : $repository['version'];
                    break;
                }
            }
        }

        return $versionReturn;
    }

    public function update()
    {

        throw new \Exception("Aun no disponible en esta version, trate de actualizar manualmente desde el composer.");

        //check if has permission to write on root directory
        if (is_writable(ABS_PATH)) {


            //Download update
            $app = App::getInstance();

            $updater = $app->getSysOption('updaterSource');

            $result = $app->lib->curlBinary($updater[0]['valor'] . "download/");

            if (file_put_contents(ABS_PATH . "Bootstrap.zip", $result)) {

                //Copies current Bootstrap directory to a backup folder
                if (rename(ABS_PATH . "Bootstrap", ABS_PATH . "Bootstrap" . date("YmdHi"))) {

                    if (!$app->lib->zipExtract(ABS_PATH . "Bootstrap.zip", ABS_PATH, true)) {
                        rename(ABS_PATH . "Bootstrap" . date("YmdHi"), ABS_PATH . "Bootstrap");
                        throw new \Exception("No se puede hacer la actualizacion, no se pudo extraer la actualizacion.");
                    } else {
                        //Se realiza con exito la actualizacion
                        header("Location: " . ABS_URL);
                        exit;
                    }
                } else {
                    throw new \Exception("No se puede hacer la actualizacion, no se pudo hacer backup.");
                }
            } else {
                throw new \Exception("No se puede hacer la actualizacion, no se pudo descargar la actualizacion.");
            }

            exit();
        } else {
            throw new \Exception("No se puede hacer la actualizacion, no hay permisos de escritura.");
        }
    }

    public function checkUpdates()
    {

        //Para evitar la recursividad
        $router = Router::getInstance();

        if ($router->getController() == "sys__easyupdate")
            return false;


        $checkIt = true;
        $versionToUpgrade = "";

        $security = Security::getInstance();
        $uniquePath = $security->getSetupString();

        $absPath = "/tmp/easyengine/$uniquePath/";
        if (!file_exists($absPath) && !mkdir($absPath, 0755, true)) {//Se crea si no existe
            throw new \Exception("No se pueden crear directorios auxiliares para actualizaciones.");
        }

        //Archivo de cache para checkeo de versiones
        $fileToCheck = $absPath . "update";
        if (file_exists($fileToCheck)) {
            $dataOfVersion = file_get_contents($fileToCheck);
            $json = json_decode($dataOfVersion, true);
            if (is_array($json) && isset($json['date']) && $json['date'] == date("Y-m-d")) {
                $checkIt = false; // Ya se comprobo hoy si hay actualizaciones
                $versionToUpgrade = $json['versionAvailable'];
            }
        }

        if ($checkIt) {

            //Si composer existe

            $currentVersion = $this->getCurrentVersion();

            if (strpos($currentVersion,'dev-') === false ) { //Si no es algun branch

                //Se obtiene la ultima version de easypanel
                $response = $this->app->lib->curl("https://api.bitbucket.org/2.0/repositories/easypanelteam/easyengine/refs/tags");
                $responseJson = json_decode($response, true);

                $lastVersion = end($responseJson['values']);

                if ($this -> version_compare($currentVersion, $lastVersion['name']) == 1) {
                    $versionToUpgrade = $lastVersion['name'];
                }
            }

            $dataToSave = array("date" => date("Y-m-d"), "versionAvailable" => $versionToUpgrade);
            file_put_contents($fileToCheck, json_encode($dataToSave));

        }

        if (!empty($versionToUpgrade)) $this->app->set("_updateAvailable", $versionToUpgrade);
    }


    /**
     * Comparacion de versiones
     * $v1 > $v2 -> -1
     * $v1 < $v2 -> 1
     * $v1 = $v2 -> 0
     *
     * @param $v1
     * @param $v2
     * @return int
     */
    private function version_compare($v1, $v2)
    {

        $versions = array(
            'v1' => explode(".", $v1),
            'v2' => explode(".", $v2)
        );

        //Mayor a Menor
        $asociaciones = array('a', 'b', 'rc', '');


        $larger = count($versions['v1']) >= count($versions['v2']) ? count($versions['v1']) : count($versions['v2']);

        for ($i = 0; $i < $larger; $i++) {

            $v1 = isset($versions['v1'][$i]) ? (strpos($versions['v1'][$i], '-') ? explode('-', $versions['v1'][$i]) : array($versions['v1'][$i], $asociaciones[0])) : array(0, $asociaciones[0]);
            $v2 = isset($versions['v2'][$i]) ? (strpos($versions['v2'][$i], '-') ? explode('-', $versions['v2'][$i]) : array($versions['v2'][$i], $asociaciones[0])) : array(0, $asociaciones[0]);

            if ($v1[0] == $v2[0]) {

                $v1slash = array_search($v1[1], $asociaciones) !== false ? array_search($v1[1], $asociaciones) : 0;
                $v2slash = array_search($v2[1], $asociaciones) !== false ? array_search($v2[1], $asociaciones) : 0;

                if ($v1slash > $v2slash) {
                    return -1;
                } else if ($v1slash < $v2slash) {
                    return 1;
                }

            } else if ($v1[0] > $v2[0]) {
                return -1;
            } else {
                return 1;
            }

        }

        return 0;

    }

}