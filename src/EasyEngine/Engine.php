<?php
/**
 * Archivo de Motor de Easy Panel
 *
 * @author Federico Romero <federicorp@easypanel.com>
 * @package EasyEnginePHP
 * @copyright 2016
 */


namespace EasyEngine;

use EasyEngine\Classes\Router;
use EasyEngine\Classes\Route;
use Symfony\Component\Debug\Debug;
use \EasyEngine\Classes as Classes;

class Engine
{

    private $confs = array(
        "configFile" => "config.inc.php"
    );

    public function __construct($config = array())
    {
        foreach ($config as $key => $value) {
            $this->confs[$key] = $value;
        }

        //  **** Autoload definido para Easypanel ****
        spl_autoload_register(array($this, "easy_autoload"));


        //  ****    Error Handler    ****
        $errorHandler = new Debug();
        $errorHandler->enable();

        $protocol = isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ? "https": "http";

        //Define Constantes de URL y PATH del sitio
        if (!defined("ABS_URL") && isset($_SERVER['HTTP_HOST']))
            define("ABS_URL", "$protocol://" . $_SERVER['HTTP_HOST'] . str_replace("public/index.php", "", $_SERVER['PHP_SELF']));
        else
            define("ABS_URL", "");

        if (!defined("ABS_PATH")) {
            $trace = debug_backtrace();
            $caller = end($trace);

            if (!isset($caller['file']))
                throw  new \Exception("No se puede saber el path absoluto para incluir archivos.");

            $directory = str_replace(DIRECTORY_SEPARATOR, "/", dirname($caller['file']));
            $piecesPATH = explode("/", $directory);
            // Asumiendo que el caller esta dentro de un directorio llamado public se quita ese directorio
            array_pop($piecesPATH);
            $directory = implode("/", $piecesPATH);


            define("ABS_PATH", $directory . "/");
        }

        $includeConfFile = ABS_PATH . $this->confs["configFile"];
        
        //  ****    Comprueba si el archivo de configuracion existe    ****
        if (!file_exists($includeConfFile)) {
            //throw new \Exception("El archivo de configuraciones aun no esta en la carpeta. Configure el sistema y luego vuelva a probar");
            if ((!isset($_GET['_url']) && strpos("__easyinstall", $_GET['_url']) !== false) || !isset($_GET['__installengine'])) {
                header("Location: " . ABS_URL . "__easyinstall?__installengine");
                exit;
            } else {
                $includeConfFile = __DIR__ . "/config.sample.inc.php";
            }
        }
       
        //  ****    Inclusion de las librerias a usarse     ****
        $app = Classes\App::getInstance();

        //Debuging
        if (isset($_GET['debug']))
            $app->setDebugging();
        
        //  ****    Incluye el archivo de configuraciones    ****
        $app->setCfg(include_once($includeConfFile));

        //Date Handling
        date_default_timezone_set(!empty($app->getCfg("date_timezone")) ? $app->getCfg("date_timezone") : 'UTC');

        $app->setBDConection();

        $app->initializeLibraries();

    }

    //  ****    Autoload de Clases del usuario (App)    ****
    public function easy_autoload($class)
    {

        $dir = "";


        $file = ABS_PATH . str_replace("\\", "/", $class) . ".php";

        if (strpos($class, "\\") !== false) {
            $dir = $file;
        }

        if (!empty($dir) && file_exists($dir)) {

            require_once($dir);
        }
    }

    public function dispatch($routes = array())
    {

        if (!is_array($routes) || count($routes) == 0) {
            $route = Route::getInstance();
            if (file_exists(ABS_PATH . "App/routes.php"))
                include_once ABS_PATH . "App/routes.php";
        } else {
            Router::getInstance()->setRoutes($routes);
        }

        Router::getInstance()->dispatch();

    }


    /**
     * Singleton
     *
     */

    private static $instancia;

    /**
     * @param array $config
     * @return Engine
     */
    public static function build($config = array())
    {
        if (!self::$instancia instanceof self) {
            self::$instancia = new self($config);
        }
        return self::$instancia;
    }

    public function __clone()
    {
        trigger_error("Operación Invalida: No puedes clonar una instancia de " . get_class($this) . " class.", E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error("No puedes deserializar una instancia de " . get_class($this) . " class.");
    }

}