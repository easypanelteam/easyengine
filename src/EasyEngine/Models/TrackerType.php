<?php

namespace EasyEngine\Models;

class TrackerType extends BaseModel 
{

    protected $table = "tracker_type_actions";

    public $timestamps = false;

    protected $primaryKey = "trctpacId";

    public $incrementing = false;

    public function levels() {
        return $this->hasMany('EasyEngine\Models\TrackerLevels');
    }



}