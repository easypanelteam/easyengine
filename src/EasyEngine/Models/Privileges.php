<?php
/**
 * Modelo de Privileges
 *
 * @author Federicorp
 */

namespace EasyEngine\Models;

class Privileges extends BaseModel
{

    protected $table = "privileges";

    public $timestamps = false;

    protected $primaryKey = "privId";

    public function users() {
        return $this->belongsToMany(\EasyEngine\Models\Users::class, "users_privileges");
    }

    public function permissions() {
        return $this->belongsToMany(\EasyEngine\Models\Permissions::class, 'privileges_permissions');
    }

}