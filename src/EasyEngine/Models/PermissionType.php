<?php
/**
 * Modelo de PermissionType
 *
 * @author Federicorp
 */

namespace EasyEngine\Models;

class PermissionType extends BaseModel
{

    protected $table = "permission_type";

    public $timestamps = false;

    protected $primaryKey = "typermId";

    public $incrementing = false;

}