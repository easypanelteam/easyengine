<?php

namespace EasyEngine\Models;

class SysOptions extends BaseModel {

    protected $table = "sys_options";

    public $timestamps = false;

    protected $primaryKey = "optId";

    public $incrementing = false;

}