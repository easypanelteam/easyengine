<?php

namespace EasyEngine\Models;

class TrackerLevels extends BaseModel 
{

    protected $table = "tracker_levels";

    public $timestamps = false;

    protected $primaryKey = "trclvlId";

    public $incrementing = false;


}