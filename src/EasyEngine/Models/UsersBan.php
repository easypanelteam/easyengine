<?php

namespace EasyEngine\Models;

class UsersBan extends BaseModel 
{

    protected $table = "users_ban";

    public $timestamps = false;

    protected $primaryKey = "banId";

    public function users() {
        return $this->hasMany('EasyEngine\Models\Users');
    }

}