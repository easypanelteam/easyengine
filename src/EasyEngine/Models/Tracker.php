<?php

namespace EasyEngine\Models;

class Tracker extends BaseModel 
{

    protected $table = "tracker";

    public $timestamps = false;

    protected $primaryKey = "trcId";

    public function users() {
        return $this->hasMany('EasyEngine\Models\Users');
    }

    public function type() {
        return $this->hasMany('EasyEngine\Models\TrackerType');
    }


}