<?php
/**
 * Modelo Base de los modelos
 *
 * @author Federicorp
 */

namespace EasyEngine\Models;

use Illuminate\Database\Eloquent\Model;
use \EasyEngine\Classes\App;

abstract class BaseModel extends Model
{
    
    public function __construct() 
    {
        
        $app = App::getInstance();
        
        if ($app->getCfg("USE_BD") !== true)
            return false;
        
        // parent::__contruct();
        
    }
    
}