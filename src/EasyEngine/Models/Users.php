<?php

namespace EasyEngine\Models;

class Users extends BaseModel 
{

    protected $table = "users";

    public $timestamps = false;

    protected $primaryKey = "userId";

    public function privileges() {
        return $this->belongsToMany('EasyEngine\Models\Privileges', 'users_privileges');
    }

    public function permissions() {
        return $this->belongsToMany('EasyEngine\Models\Permissions', 'users_permissions');
    }

}