<?php
/**
 * Modelo de Permission
 *
 * @author Federicorp
 */

namespace EasyEngine\Models;

class Permissions extends BaseModel
{

    protected $table = "permissions";

    public $timestamps = false;

    protected $primaryKey = "permId";

    public $incrementing = false;

    public function types() {
        return $this->hasMany('EasyEngine\Models\PermissionType', 'typermId');
    }

    public function users() {
        return $this->belongsToMany('EasyEngine\Models\Users', 'users_permissions');
    }

    public function privileges() {
        return $this->belongsToMany('EasyEngine\Models\Privileges', 'privileges_permissions');
    }

}