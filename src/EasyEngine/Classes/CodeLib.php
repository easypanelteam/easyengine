<?php

/**
 * Clase de librerÃ­as Reutilizables
 *
 * @package EasyPHP
 * @author Federico Romero <federicorp@easypanelphp.com>
 * @copyright 2014
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */


namespace EasyEngine\Classes;

use \ZipArchive, \RecursiveIteratorIterator, \RecursiveDirectoryIterator;

class CodeLib extends VarsHandle
{

    protected $app;

    public function __construct()
    {
        $this->app = app::getInstance();
    }

    public function getFechaHora()
    {

        $timezone = $this->app->getSysOption("GMT_sys");
        if ($timezone !== false) {
            date_default_timezone_set($timezone);
            return date("Y-m-d H:i:s");//Retorna la Hora de acuerdo al GMT configurado
        } else {
            return date("Y-m-d H:i:s");//Retorna la hora del servidor
        }

    }

    public function getRealIP()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];

        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];

        return $_SERVER['REMOTE_ADDR'];
    }

    public function getRemoteIP()
    {
        if (!empty($_SERVER['REMOTE_ADDR']))
            return $_SERVER['REMOTE_ADDR'];

        return '';
    }

    public function getUserAgent()
    {
        return "";
    }

    /* creates a compressed zip file */
    public function zipCreate($files = array(), $destination = '', $overwrite = false)
    {

        //if the zip file already exists and overwrite is false, return false
        if (file_exists($destination) && !$overwrite) {
            return false;
        }

        //vars
        $valid_files = array();
        //if files were passed in...
        if (is_array($files)) {
            //cycle through each file
            foreach ($files as $file) {
                //make sure the file exists
                if (file_exists($file)) {
                    if (is_dir($file)) {
                        $recursive = new RecursiveIteratorIterator(
                            new RecursiveDirectoryIterator($file),
                            RecursiveIteratorIterator::LEAVES_ONLY
                        );

                        foreach ($recursive as $name => $fileToZip) {
                            // Skip directories (they would be added automatically)
                            if (!$fileToZip->isDir()) {
                                // Get real and relative path for current file
                                $valid_files[] = $name;

                            }
                        }


                    } else {
                        $valid_files[] = $file;
                    }
                }
            }
        }

        //if we have good files...
        if (count($valid_files)) {

            if (extension_loaded('zip')) {

                //create the archive
                $zip = new \ZipArchive();
                if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                    return false;
                }

                //add the files
                foreach ($valid_files as $file) {
                    if (is_dir($file)) {
                        $zip->addGlob($file . "/*");
                    } else {
                        $zip->addFile($file, $file);
                    }
                }

                //close the zip -- done!
                $zip->close();

            } else {
                return false;
            }

            //check to make sure the file exists
            return file_exists($destination);

        } else {
            return false;
        }

    }

    public function zipExtract($file, $path, $deleteFile = false)
    {

        if (extension_loaded('zip')) {
            $zip = new \ZipArchive();

            $res = $zip->open($file);

            if ($res === TRUE) {

                if (!$zip->extractTo($path)) {
                    $zip->close();
                    return false;
                }
                $zip->close();
                if ($deleteFile) unlink($file);
                return true;
            } else {
                return false;
            }
        } else {
            `cd $path && unzip $file && rm $file`;
        }

    }

    public function force_download($file, $path = '')
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path . $file));
        readfile($path . $file);
        exit;
    }

    public function curlBinary($url, $data = array()) {
        $this -> curl($url,$data, array(CURLOPT_BINARYTRANSFER=>1));
    }

    public function curl($url, $data = array(), $opt = array())
    {
        $result = false;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if ((is_array($data) && count($data) > 0) || (is_string($data) && !empty($data))) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        foreach ($opt as $key => $value) {
            curl_setopt($ch, $key, $value);
        }
        $response = curl_exec($ch);

        if ($this->app->debug) {
            echo "<!-- llama a la URL $url -->";
        }

        if (strlen(curl_error($ch)) > 0) {
            if ($this->app->debug) {
                echo "<!-- Curl, error en la peticion -->";
            }
        } else {
            $result = $response;
        }

        return $result;
    }

    public function escape_includes($include)
    {
        return str_replace("/", DIRECTORY_SEPARATOR, $include);
    }

    /**
     * @return string
     */
    public function getCurURL()
    {
        $protocol = isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ? "https": "http";

        $url = "$protocol://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        return $url;
    }

}
    