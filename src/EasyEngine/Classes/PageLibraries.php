<?php

namespace EasyEngine\Classes;

class PageLibraries extends VarsHandle
{

    public function addCssLib($path, $name = "")
    {

        $this->addLibs("CSS", $path, $name);
    }

    public function getAllCssLibs()
    {
        return $this->getLibs("CSS");
    }

    public function addJsLib($path, $name = "")
    {
        $this->addLibs("JS", $path, $name);
    }

    public function getAllJsLibs()
    {
        return $this->getLibs("JS");
    }

    private function addLibs($type, $path, $name)
    {

        $libraries = $path;
        if (!is_array($path)) {
            $libraries = array($name => $path);
        }

        foreach ($libraries as $name => $path) {

            $nameTosave = strtoupper($type) . "LIB_" . $name; //Para saber que tipo de libreria es

            $pathComplete = strpos($path, '//') !== false ? $path : ABS_URL . $path; //En caso de ser libreria externa

            if ($this->exists($nameTosave)) {
                $pathExploded = explode("/", $path);

                $nameTosave .= end($pathExploded);
            }

            $this->set($nameTosave, $pathComplete);
        }
    }

    private function getLibs($type)
    {
        $result = array();

        foreach ($this->getAll() as $name => $path) {
            if (strpos($name, strtoupper($type) . "LIB_") === 0) {
                $result[] = $path;
            }
        }

        return $result;
    }

}