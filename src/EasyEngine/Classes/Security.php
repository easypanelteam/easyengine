<?php
/**
 * Clase de seguridad del sistema
 *
 * @package EasyEnginePHP
 * @author Federicorp <federicorp@easypanelphp.com>
 * @copyright 2013
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

namespace EasyEngine\Classes;

class Security
{


    public function clean_globals($exceptions = array())
    {
        if (!ini_get('register_globals'))
            return;

        if (isset($_REQUEST['GLOBALS']))
            die('GLOBALS overwrite attempt detected');

        // Variables that shouldn't be unset
        $no_unset = array('GLOBALS', '_GET', '_POST', '_COOKIE', '_REQUEST', '_SERVER', '_ENV', '_FILES', 'table_prefix');

        $input = array_merge($_GET, $_POST, $_COOKIE, $_SERVER, $_ENV, $_FILES, isset($_SESSION) && is_array($_SESSION) ? $_SESSION : array());
        foreach ($input as $k => $v) {
            if (!in_array($k, $no_unset) && isset($GLOBALS[$k])) {
                $GLOBALS[$k] = null;
                unset($GLOBALS[$k]);
            }
        }
    }

    /**
     * Funcion para preparar el encriptado de la contraseÃ±a.
     * Posibles valores para el $mode: 'strrev','normal','no-salt','md5-md5'
     * Predeterminado 'strrev'.
     *
     * @param string , mode
     * @return crypt() del string
     */

    public function prep_encr($string, $mode = "strrev")
    {
        if ($mode == "strrev")
            return crypt($string, strrev($string));
        if ($mode == "normal")
            return crypt($string, $string);
        if ($mode == "no-salt")
            return crypt($string);
        if ($mode == "md5-md5")
            return md5(md5($string));
        if ($mode == "md5-strrev")
            return md5(crypt($string, strrev($string)));
        if ($mode == "strrev-md5") {
            $md5 = md5($string);
            return crypt($md5, strrev($md5));
        }
    }

    /**
     * Metodo para obtener el string unico la instalacion. (Depende del ENC_SALT de la configuracion)
     *
     * @return string
     */
    public function getSetupString()
    {
        $app = App::getInstance();

        $enc_salt = $app->getCfg("ENC_SALT");

        return $this->prep_encr($enc_salt,'md5-md5');
    }

    /**
     * Singleton
     */
    private static $instancia;

    public static function getInstance()
    {
        if (!self::$instancia instanceof self) {
            self::$instancia = new self;
        }
        return self::$instancia;
    }

    public function __clone()
    {
        trigger_error("OperaciÃ³n Invalida: No puedes clonar una instancia de " . get_class($this) . " class.", E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error("No puedes deserializar una instancia de " . get_class($this) . " class.");
    }

}


?>