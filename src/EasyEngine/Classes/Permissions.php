<?php

/**
 * Archivo de funciones para permisos de usuarios
 *
 * @package EasyEnginePHP
 * @author Federicorp <federicorp@easypanel.com>
 * @copyright 2016
 *
 */

namespace EasyEngine\Classes;

use EasyEngine\Models\Permissions as Model;
use EasyEngine\Models\Users as ModelUser;
use EasyEngine\Models\Privileges as ModelPrivileges;
use EasyEngine\Models\PermissionType as ModelType;

class Permissions
{

    /**
     * @var App
     */
    protected $app;

    /**
     * Variable cache de permisos del usuario
     *
     * @var array
     */
    private $permisosMat = array();

    /**
     * @var bool
     */
    public $bool_permisos = FALSE;

    public function __construct()
    {
        $this->app = App::getInstance();

        // Solo si hay BD definida y si se usan los permisos
        if ($this->app->db !== false && $this->app->getCfg("USE_PERMISSIONS") !== null && $this->app->getCfg("USE_PERMISSIONS") && $this->app->user->isLogged())
            $this->getAllPermisosToUser();
    }


    /**
     * Comprueba si el usuario tiene varios permisos a la vez
     *
     * @param string|array $permisos
     * @return boolean
     */

    public function checkIf($permisos)
    {

        if (is_string($permisos)) $permisos = explode(',', $permisos);


        foreach ($permisos as $permiso) {
            if (!$this->havePermission($permiso))
                return false;
        }

        return true;
    }


    /**
     * Retorna si tiene o no permisos para realizar las acciones
     *
     * @param string $permiso
     * @return boolean
     */

    public function havePermission($permiso)
    {
        return isset($this->permisosMat[$permiso]);
    }

    private function getAllPermisosToUser()
    {

        // Gets All Permission exclusive for the user
        $permisosUser = ModelUser::find($this->app->user->getId())->permissions()->get()->all();
        foreach ($permisosUser as $value) {
            $this->permisosMat[$value->permId] = true;
        }

        // Privilege of the user
        $permisosPrivilegesUser = ModelUser::with('privileges.permissions')->where("userId",$this->app->user->getId())->get();
        foreach ($permisosPrivilegesUser as $key=>$privilege) {
            foreach ($privilege->first()->privileges->first()->permissions->all() as $permission) {
                $this->permisosMat[$permission->permId] = true;
            }
        }

    }

    /**
     * Funcion para obtener los permisos de la BD
     *
     * @since 0.1.1
     */
    public function getAllPermisos()
    {

        $result = Model::with('types')->orderBy("typermId", "ASC")->get()->all();

        return $result;
    }

    public function addPermiso($permId, $permDetail, $typermId)
    {
        return $this->db->save("permId, permDetail, typermId", "permission", "?,?,?", $permId, $permDetail, $typermId);
    }

    public function deletePermiso($permId)
    {
        return $this->db->query("DELETE FROM permission WHERE permId='" . $this->db->clean($permId) . "'");
    }

    public function getAllPermisosType()
    {
        $result = false;

        $result = ModelType::get()->all();

        return $result;
    }

    public function addPermisoType($detail)
    {
        return $this->db->save("typermDetail", "permission_type", "?", $detail);
    }

    public function deletePermisoType($typermId)
    {
        return $this->db->query("DELETE FROM permission_type WHERE typermId='" . $this->db->clean($typermId) . "'");
    }

    /**
     * Singleton
     */
    private static $instancia;

    /**
     * @return permissions
     */
    public static function getInstance()
    {
        if (!self::$instancia instanceof self) {
            self::$instancia = new self();
        }
        return self::$instancia;
    }

    public function __clone()
    {
        trigger_error("Operación Invalida: No puedes clonar una instancia de " . get_class($this) . " class.", E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error("No puedes deserializar una instancia de " . get_class($this) . " class.");
    }

}
