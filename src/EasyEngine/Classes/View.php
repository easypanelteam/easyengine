<?php
/**
 * Archivo para manipulacion de datos en el view
 *
 * @package EasyEnginePHP
 * @author Federicorp <federicorp@easypanelphp.com>
 * @copyright 2016
 *
 */

namespace EasyEngine\Classes;

use Twig_Loader_Filesystem,Twig_Environment;

class View extends VarsHandle
{

    private $twig;
    private $pathToTemplates;
    private $app;
    private $templates;

    public function __construct($templates, $pathToTemplates = "./")
    {
        
        if ($pathToTemplates == "./")
            $pathToTemplates = file_exists(ABS_PATH . "App/Views/") ? ABS_PATH . "App/Views/" : $pathToTemplates;
        
        $this->templates = $templates;
        
        $this->pathToTemplates = $pathToTemplates;
        $loader = new Twig_Loader_Filesystem($pathToTemplates);
        $this->twig = new Twig_Environment($loader);
        $this->app = App::getInstance();
    }
    
    

    public function renderIt()
    {

        //Ver cual de los templates cargar
        $templateToRender = "";
        
        if (!is_array($this->templates))
            $this->templates = [$this->templates];
        
        foreach ($this->templates as $template) {
            
            // Si tiene puntos significan carpetas.
            if (strpos($template, ".") !== false) {
                $template = str_replace(".", "/", $template);
            }
            
            if ($this->viewExists($this->app->lib->escape_includes($template . ".twig"))) {
                $templateToRender = $this->app->lib->escape_includes($template . ".twig");
                break;
            }
            
            // DEPRECATED
            /*if ($this->viewExists($this->app->lib->escape_includes($template . ".phtml"))) {
                $templateToRender = $this->app->lib->escape_includes($template . ".phtml");
                break;
            }*/
        }

        $this->setVar('view', $this);
        $this->setVar("CodeLib", $this->app->lib);
        $this->setVar("user", $this->app->user);
        $this->setVar("ABS_URL", ABS_URL);

        if (!empty($templateToRender)) {
            echo $this->twig->render($templateToRender, $this->getAll());
        } else {
            if (isset($_GET["debug"]))
                throw new \Exception("404 - View Not Found. " . print_r($this->templates, true));
        }
    }


    public function getHeader()
    {
        $route = Route::getInstance();

        $controller = $route->controller;
        $action = $route->action;

        $templates = array($controller . "/" . $action . "Header", $controller . "_" . $action . "Header", $controller . "/header", $controller . "Header", $action . "Header", "header");

        echo $this->renderIt($templates);

    }

    public function getFooter()
    {
        $route = Route::getInstance();

        $controller = $route->controller;
        $action = $route->action;

        $templates = array($controller . "/" . $action . "Footer", $controller . "_" . $action . "Footer", $controller . "/footer", $controller . "Footer", $action . "Footer", "footer");

        echo $this->renderIt($templates);
    }

    public function setVar($name, $value)
    {
        parent::set($name, $value);
    }
    
    public function setVars($vars)
    {
        foreach ($vars as $name => $value)
            parent::set($name, $value);
    }

  
    public function viewExists($template) {
        return file_exists($this->pathToTemplates . $template);
    }

}