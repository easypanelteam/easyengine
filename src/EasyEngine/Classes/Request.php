<?php

namespace EasyEngine\Classes;


class Request
{

    private $params;


    public function setParam($name, $value = "")
    {
        if (is_array($name))
            foreach ($name as $key => $val)
                $this->params[$key] = $val;
        else
            $this->params[$name] = $value;
    }

    public function getParam($name = "")
    {
        if (!empty($name))
            return isset($this->params[$name]) ? $this->params[$name] : null;
        return $this->params;
    }

}