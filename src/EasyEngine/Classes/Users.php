<?php
/**
 * Clase de Usuarios del sistema
 *
 * @package EasyEnginePHP
 * @author Federicorp <federicorp@easypanelphp.com>
 * @copyright 2013 Federicorp
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

namespace EasyEngine\Classes;

use EasyEngine\Models\Users as Model;

class Users
{

    /**
     * Variable de instancia del App
     *
     * @var App
     */
    protected $app;

    /**
     * Configuraciones por default a ser modificadas de acuerdo a la base de datos y configuraciones deseadas
     *
     * @var array
     */
    public $conf = array(

        /* Session timeout in seconds */
        'sessttl' => 3600, // Una hora por defecto

        /* Encriptacion de pass */
        "encriptacion" => true,
        "encType" => "strrev-md5",


        /* Variables session*/
        "nombre_session_nick" => "usuario",
        "nombre_session_pass" => "pass_usuario",
        "nombre_session_id" => "id",
        "guardar_session_pass" => true,


        /* Campos de la base de datos */
        "tabla" => "users",
        "campo_id" => "userId",
        "campo_nickname" => "userNickName",
        "campo_pass" => "userPass",
        "campo_mail" => "userEmail",
        "campo_baja_user" => "userActive",
        "campo_fecha_Registro" => "userDateSignIn",

    );


    /**
     * Nick Name del Usuario
     *
     * @var null|string
     */
    private $nickname = null;


    /**
     * Pass encriptado del usuario
     *
     * @var null|string
     */
    private $pass = null;

    /**
     * Id en la BD del Usuario
     *
     * @var null|string
     */
    public $id = null;

    /**
     * @var Security
     */
    protected $security;

    protected $model;
    
    /**
     * Users constructor.
     */
    public function __construct()
    {
        
        //Libraries
        $this->includes();

        $this->model = new Model();
        
        // Se agarran las configuraciones para las tablas
        $confs = $this->app->getCfg("USERS_BD_CONF");
        if ($confs != null && is_array($confs)) {
            $this->model = parent::__construct($this->model, $confs);
            $this->conf = array_merge($this->conf, $confs);
        }


        // server should keep session data for sessttl seconds
        ini_set('session.gc_maxlifetime', $this->conf['sessttl']);

        // each client should remember their session id for Exactly sessttl seconds
        session_set_cookie_params($this->conf['sessttl']);


        if (!isset($_SESSION))
            session_start();


        // Destruir la sesion si pasan los X minutos
        if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > $this->conf['sessttl'])) {
            // last request was more than sessttl minutes ago
            session_unset();     // unset $_SESSION variable for the run-time
            session_destroy();   // destroy session data in storage
        }
        $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp


        if ($this->isLogged()) {
            $this->nickname = $_SESSION[$this->conf['nombre_session_nick']];
            $this->id = $_SESSION[$this->conf['nombre_session_id']];
            if ($this->conf["guardar_session_pass"]) {
                //Inseguro
//                if($row = $this->BD->fetch($this->BD->query("SELECT * FROM ".$this->conf["tabla"]." WHERE ".$this->conf["campo_id"]."='".$this->BD->clean($this->id)."'")))
//                    $this->pass = $row[$this->conf['nombre_session_pass']];
            }
        }
    }


    /**
     * Metodo que incluye las librerias a usarse dentro de la clase
     *
     * @return void
     */
    protected function includes()
    {
        $this->security = Security::getInstance();
        $this->app = App::getInstance();
    }

    /**
     * Funcion para el login, retorna true si los datos son correctos
     * y false si no. Setea las variables $_SESSION.
     *
     * @param $nick string
     * @param $pass string
     * @param bool $useEncrypt
     * @return bool
     */

    public function login($nick, $pass, $useEncrypt = true)
    {

        //Si se accede con el user de la BD
        if ($this->app->getCfg("USE_BD_AS_LOGIN")) {

            //Se obtienen datos para conectar a la BD
            $conf = $this->app->getCfg("CONF_LOGIN_BD");
            $conf["USER"] = $nick;
            $conf["PASS"] = $pass;
            $nameConn = "userConn";

            $this->app->dbmanager->addConn($conf, $nameConn);

            //Se guarda el numero de conexion en el id del usuario
            //$this->id = $nameConn;

            //Comprobar si no se tuvo error al conectar a la BD
            if ($this->app->dbmanager->isConnected($nameConn)) {
                $_SESSION[$this->conf['nombre_session_nick']] = $nick;
                $_SESSION[$this->conf['nombre_session_id']] = $nameConn;
                if ($this->conf["guardar_session_pass"])
                    $_SESSION[$this->conf['nombre_session_pass']] = $pass;
                return true;
            }
            return false;
        } else {

            if ($useEncrypt)
                $pass = $this->security->prep_encr($pass, $this->conf['encType']);

            $user = $this->model->where([
                [$this->conf['campo_nickname'], "=", $nick],
                [$this->conf['campo_pass'], "=", $pass]
            ]);

            if ($user->count() == 1) {
                $row = $user->select([$this->conf['campo_id']])->first();

                $campoId = $this->conf['campo_id'];
                $_SESSION[$this->conf['nombre_session_nick']] = $nick;
                $_SESSION[$this->conf['nombre_session_id']] = $row->$campoId;
                $this->id = $row->$campoId;
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Funcion para el registro, retorna 1 si los datos son guardados correctamente.
     * 0 = error de mysql al insertar el usuario
     * 2 = ya existe el nickname
     * 3 = ya existe el mail registrado
     *
     * @param $nick
     * @param $pass
     * @param $mail
     * @return int
     * @since 17-02-2016
     */

    public function register($nick, $pass, $mail)
    {

        //Se encripta el pass
        $pass = $this->security->prep_encr($pass, $this->conf['encType']);

        //Comprueba existencia del nick
        if ($this->model->where($this->conf['campo_nickname'], $nick)->count() == 0) {

            // Comprueba existencia del mail
            if ($this->model->where($this->conf['campo_mail'], $mail)->count() == 0) {

                // inserta en la tabla
                try{

                    $campoNickname = $this->conf['campo_nickname'];
                    $campoPass = $this->conf['campo_pass'];

                    $userToSave = new Model();
                    $userToSave->$campoNickname = $nick;
                    $userToSave->$campoPass = $pass;

                    $userToSave->save();

                    return 1;

                }catch (\Exception $e) {
                    return 0;
                }

            } else {
                return 3;
            }
        } else {
            return 2;
        }
    }


    /**
     * Funcion para saber si el usuario existe en la base de datos
     *
     * @param string $valor
     * @param string $mode
     * @return bool
     */

    public function exists($valor, $mode = "id")
    {
        if ($mode == "id") {
            if ($this->model->find($valor)->count() > 0)
                return true;
            return false;
        } else if ($mode == "nombre") {
            if ($this->model->where($this->conf['campo_nickname'], $valor)->count() > 0)
                return true;
            return false;
        } else if ($mode == "email") {
            if ($this->model->where($this->conf['campo_mail'], $valor)->count() > 0)
                return true;
            return false;
        }
    }

    /**
     * Funcion para saber si el pass fue reestablecido
     *
     * @param $nick
     * @return bool
     */

    public function isPassReseted($nick)
    {
        if ($this->app->getCfg("USE_BD_AS_LOGIN"))
            return false;


        $user = $this->model->where($this->conf['campo_nickname'], "=", $nick);

        $sql = $user->select($this->conf['campo_baja_user'], $this->conf['campo_pass'])->get()->all();

        if (count($sql) > 0) {
            $row = $sql[0];
            $campoBajaUser = $this->conf['campo_baja_user'];
            $campoPass = $this->conf['campo_pass'];
            if ($row->$campoBajaUser == 0) {
                if (strlen($row->$campoPass) == 6) {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    /**
     * Funcion para saber si el usuario esta logueado en el sistema
     *
     * @param none
     * @return true si estÃƒÂ¡ logeado
     */

    public function isLogged()
    {
        return isset($_SESSION[$this->conf['nombre_session_id']]);
    }

    /**
     * Retorna el nombre del usuario
     *
     * @return string
     */

    public function get_nombre()
    {
        return $this->nickname;
    }

    /**
     * Retorna el id del usuario
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Cierra la sesion
     *
     * @return string
     */

    public function cerrarSesion()
    {
        unset($_SESSION[$this->conf["nombre_session_nick"]], $_SESSION[$this->conf["nombre_session_id"]], $_SESSION[$this->conf["nombre_session_pass"]]);
    }

    /**
     * Singleton
     */

    private static $instancia;

    public static function getInstance()
    {
        if (!self::$instancia instanceof self) {
            self::$instancia = new self;
        }
        return self::$instancia;
    }

    public function __clone()
    {
        trigger_error("Operacion Invalida: No puedes clonar una instancia de " . get_class($this) . " class.", E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error("No puedes deserializar una instancia de " . get_class($this) . " class.");
    }
}