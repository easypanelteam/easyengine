<?php

/**
 * Clase de acciones para el EasyApp
 *
 * @package EasyEnginePHP
 * @author Federico Romero <federicorp@easypanelphp.com>
 * @copyright 2014
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

namespace EasyEngine\Classes;

use EasyEngine\Models\SysOptions as SysModel;

class App extends VarsHandle
{

    /**
     * @var DbManager
     */
    public $dbmanager = NULL;

    /**
     * @var \Illuminate\Database\Connection
     */
    public $db = null;

    /**
     * @var Page
     */
    public $page;

    /**
     * @var Users
     */
    public $user;

    /**
     * @var Tracker
     */
    public $tracker;

    /**
     * @var Permissions
     */
    public $permisos;

    /**
     * @var boolean
     */
    public $debug;

    /**
     * @var CodeLib
     */
    public $lib;

    /**
     * @var array
     */
    private $cfg;

    /**
     * @var Cache
     */
    public $cache;

    public function __construct()
    {


    }

    public function initializeLibraries()
    {
        $this->user = Users::getInstance();
        $this->page = Page::getInstance();
        $this->tracker = Tracker::getInstance();
        $this->perm = Permissions::getInstance();
        $this->lib = new CodeLib();
        $this->cache = new Cache();
		
    }

    public function setDebugging()
    {

        //Muestra de errores
        error_reporting(E_ALL);
        ini_set("display_errors", 1);
        $this->debug = true;
    }

    public function setCfg($conf)
    {
        $this->cfg = $conf;
    }

    public function getCfg($conf = "")
    {

        if ($conf == '')
            return $this->cfg;
        if (isset($this->cfg[$conf]))
            return $this->cfg[$conf];

        return NULL;
    }

    public function setBDConection()
    {
        $this->dbmanager = new DbManager();
        $this->dbmanager->setConf($this->cfg);
        $this->db = $this->dbmanager->getConn();

        //si es debug
        if ($this->debug && $this->getCfg("USE_BD"))
            $this->db->enableQueryLog();
    }

    /**
     * Obtiene una opcion del sistema guardada en BD
     *
     * @param string $option
     * @return mixed
     */
    public function getSysOption($option = '') 
	{

		if ($this->getCfg("USE_BD") !== true)
			return false;
		
        $sql = SysModel::query();

        if (!empty($option))
            $sql->where("optId", "=", $option);

        $result = $sql->get(["optValue as valor"])->first();

        if ($result === null)
            return $result;

        return $result->valor;

    }

    /**
     * Singleton
     *
     */

    private static $instancia;

    /*
	 * @return app
     */
    public static function getInstance()
    {
        if (!self::$instancia instanceof self) {
            self::$instancia = new self;
        }
        return self::$instancia;
    }

    public function __clone()
    {
        trigger_error("Operación Invalida: No puedes clonar una instancia de " . get_class($this) . " class.", E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error("No puedes deserializar una instancia de " . get_class($this) . " class.");
    }

}