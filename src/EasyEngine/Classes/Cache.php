<?php

namespace EasyEngine\Classes;

use phpFastCache\CacheManager;

class Cache
{

    /**
     * @var App
     */
    private $app;


    private $options = array(
        "cachear" => false,
        "defaults" => array(
            "cacheType" => "files",
            "cachePath" => "/tmp/easyengine/{uniquepath}/cache/",
            "cacheTtl" => 60
        )
    );

    private $InstanceCache = null;

    public function __construct()
    {

        $this->app = App::getInstance();


        $cachearOpt = $this->app->getSysOption("CACHEON");

        if ($cachearOpt !== false && $cachearOpt == 1) {

            $this->setOpt("cachear", true);


            //Setear el cacheador

            //Tipo de cache
            $cacheTypeOpt = $this->app->getSysOption("CACHETYPE");
            $cacheTypeOpt = $cachearOpt === false ? $this->getDefaultOpt('cacheType') : $cacheTypeOpt;


            $security = new Security();
            $uniquePath = $security->getSetupString();


            //Si es Cache del Tipo archivo
            if ($cacheTypeOpt == "files") {
                //Path para el cache
                $cachePathOpt = $this->app->getSysOption("CACHEPATH");
                $cachePathOpt = $cachePathOpt === false ? str_replace("{uniquepath", $uniquePath, $this->getDefaultOpt('cachePath')) : $cacheTypeOpt;

                // Setup File Path on your config files
                CacheManager::setup(array(
                    "path" => $cachePathOpt, // or in windows "C:/tmp/"
                ));
            }

            //Tiempo de vida del cache
            $cacheTtlOpt = $this->app->getSysOption("CACHEDEFAULTTL");
            $cacheTtlOpt = $cachearOpt === false ? $this->getDefaultOpt('cacheTtl') : $cacheTypeOpt;

            $this->setOpt("ttl", $cacheTtlOpt);

            $this->InstanceCache = CacheManager::getInstance($cacheTypeOpt);


        }

    }

    private function setOpt($key, $value) {
        $this->options[$key] = $value;
    }

    private function getOpt($key) {
        return isset($this->options[$key]) ? $this->options[$key] : false;
    }

    private function setDefaultOpt($key, $value) {
        $this->options['defaults'][$key] = $value;
    }

    private function getDefaultOpt($key) {
        return isset($this->options['defaults'][$key]) ? $this->options['defaults'][$key] : false;
    }

    /**
     * Comprueba que exista el key dentro del cache
     *
     * @param $key
     * @return bool
     */
    public function exists($key)
    {

        return $this->InstanceCache->getItem($key)->isHit();
    }


    /**
     * Obtiene el valor del key que se encuentra en el cache
     *
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->InstanceCache->getItem($key)->get();
    }


    /**
     * Guarda un valor en el cache
     *
     * @param $key
     * @param $value
     * @param bool $ttl
     * @return bool
     */
    public function set($key, $value, $ttl = false)
    {

        if ($ttl === false)
            $ttl = $this->getOpt("ttl");

        //Se obtiene el index
        $item = $this->InstanceCache->getItem($key)
        ->set($value)->expiresAfter($ttl);

        return $this->InstanceCache->save($item);
    }


    /**
     * Para consultar si existe, sino, guarda el valor
     *
     * @param $key
     * @param $function Funcion para obtener el valor
     * @param bool $ttl
     */
    function existsSave($key, $function, $ttl = false) {
        // .. TODO


    }


}