<?php

/**
 * Clase de Ruteo
 *
 * @package EasyEnginePHP
 * @author Federicorp <federicorp@easypanelphp.com>
 * @copyright 2014
 *
 */

namespace EasyEngine\Classes;

class Router
{

    private $controller;

    private $action;

    private $params;

    protected $app;

    protected $routes;

    protected $urlRequest;


    public function __construct()
    {

        $this->app = App::getInstance();
        $this->routes = Route::getInstance();


    }

    /**
     * Metodo para hacer el dispatch de la pagina
     *
     * @throws \Exception
     */
    public function dispatch()
    {
        // Obtencion de URL para controllers, actions y data.
        $this->urlRequest = '/index/';
        if (isset($_GET['_url'])) {
            $this->urlRequest = $_GET['_url'];
        } else if (isset($_SERVER['PATH_INFO'])) {
            $this->urlRequest = $_SERVER['PATH_INFO'];
        } else if (count($argv = ( isset($_SERVER['argv'])) ? $_SERVER['argv'] : []) > 0) {
            $this->urlRequest = $argv[1];
        }


        //Tracking
        $this->app->tracker->register("LOG_BOO", "Se recibe el request en la URL $this->urlRequest");


        // Se hace logica de recuperacion del controller y action del Route

        $routeData = $this->routes->getDataRoute($this->urlRequest);

        $action = $routeData['action'];

        $this->params = $routeData['params'];

        $this->app->tracker->register("LOG_BOO", "Accion recuperada para la ruta = " . $action);

        if ($action == "") {
            $this->app->tracker->register("LOG_BOO", "Accion de la ruta esta vacia. Ejecucion detenida.");
            throw new \Exception("Accion de ruta vacia. PROGRAM HALTED");
        }


        $this->action = "index";
        if (strpos($action, "@") || strpos($action, ":")) {
            $explArr = explode("@", $action);
            $explPunt = explode(":", $action);

            $this->controller = count($explArr) > 1 ? $explArr[0] : $explPunt[0];
            $this->action = count($explArr) > 1 ? $explArr[1] : $explPunt[1];
        }

        $this->app->tracker->register("LOG_BOO", "Controlador = " . $this->controller);
        $this->app->tracker->register("LOG_BOO", "Accion = " . $this->action);

        $this->app->page->dispatcher();
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getFullController()
    {
        // En caso que sea routing del sistema
        if (strpos($this->controller, "\\") === 0)
            return $this->controller;

        // Routing normal
        return $this->routes->getNameSpace() . $this->controller;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getFullUrl()
    {
        return $this->urlRequest;
    }

    public function getAllParams()
    {
        return $this->params;
    }

    /**
     * Para setear las rutas
     *
     * @param $routes
     * @return $this
     * @throws \Exception
     */
    public function setRoutes($routes)
    {

        throw new \Exception("Not Yet Implemented");

        if (is_array($routes) && count($routes) > 0) {

            $this->routes = $routes;

        }

        return $this;
    }

    /**
     * Singleton
     */

    private static $instancia;

    public static function getInstance()
    {
        if (!self::$instancia instanceof self) {
            self::$instancia = new self;
        }
        return self::$instancia;
    }

    public function __clone()
    {
        trigger_error("Operacion Invalida: No puedes clonar una instancia de " . get_class($this) . " class.", E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error("No puedes deserializar una instancia de " . get_class($this) . " class.");
    }

}
