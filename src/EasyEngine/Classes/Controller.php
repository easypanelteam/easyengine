<?php


namespace EasyEngine\Classes;

class Controller
{

    /**
     * @var App
     */
    public $app;

    /**
     * @var Router
     */
    public $route;

    /**
     * @var View
     */
    public $view;

    public function __construct()
    {

        $this->route = Router::getInstance();
        $this->app = App::getInstance();

        $class = get_class($this);

        if ($class != __CLASS__) {

            $fcontroller = $this;

            $faction = "__alwaysExecAction";
            if (method_exists($fcontroller, $faction))
                $fcontroller->$faction();
        }
    }

    //Trigger Before Start
    public function __beforeStart()
    {

    }

    //Trigger After Finish
    public function __afterFinish()
    {

    }

}
