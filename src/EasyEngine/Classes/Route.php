<?php

/**
 * Clase de Ruteo
 *
 * @package EasyEnginePHP
 * @author Federicorp <federicorp@easypanelphp.com>
 * @copyright 2017
 *
 */

namespace EasyEngine\Classes;

class Route
{
    /**
     * Instancia de la clase App
     * @var App
     */
    protected $app;

    /**
     * Rutas de la aplicacion
     * @var array
     */
    protected $routes = array();

    private $actualGroup = "";

    private $namespaceFiles = 'App\Controllers\\';


    /**
     * Route constructor.
     */
    public function __construct()
    {
        $this->app = App::getInstance();

        // Routing de EasyEngine

        // Installing
        $this->get("/__easyinstall", '\EasyEngine\GUI\Controllers\Installer@index');
        $this->post("/__easyinstall/install", '\EasyEngine\GUI\Controllers\Installer@install');

        // Updating
        $this->get("/__easyupdate", '\EasyEngine\GUI\Controllers\Updater@index');
        $this->get("/__easyupdate/update", '\EasyEngine\GUI\Controllers\Updater@update');

        // Status of System
        $this->get("/__easystatus", '\EasyEngine\GUI\Controllers\Status@index');

        // Default console
        $this->console("/__easyconsole", '\EasyEngine\Classes\Console');
        
    }

    /**
     * Metodo para agrupar las rutas
     *
     * @param $name
     * @param $callback
     */
    public function group($name, $callback)
    {
        $this->actualGroup = $name;
        $callback($this);
        $this->actualGroup = "";
    }

    /**
     * Metodo para definir las rutas por post
     *
     * @param $url
     * @param $action
     */
    public function post($url, $action)
    {
        $this->setRoute($url, $action, "post");
    }

    /**
     * Metodo para definir las rutas por post
     *
     * @param $url
     * @param $action
     */
    public function get($url, $action)
    {
        $this->setRoute($url, $action, "get");
    }

    /**
     * Metodo para definir las rutas por post
     *
     * @param $url
     * @param $action
     */
    public function console($url, $action)
    {
        $this->setRoute($url, $action, "console");
    }


    public function getDataRoute($url)
    {

        $method = strtolower(isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'CONSOLE');

        if (!empty($url)) {

            // Checkear si existe algo para la ruta

            foreach (array_keys($this->routes[$method]) as $route) {

                // Si tiene que ser con pregmatch
                if (strpos($route, "(.*)") !== FALSE) {
                    // Escape "/"
                    $routeEscaped = str_replace("/", "\\/", $route);
                    preg_match("/$routeEscaped/", $url, $matches);
                } else {
                    $matches = $url == $route || $url == $route . "/" ? [$route] : [];
                }

                if (count($matches) > 0) {

                    unset($matches[0]);
                    $params = array_values($matches);

                    // Se obtienen los nombres de los parametros y sus posiciones
                    $routeParams = $this->routes[$method][$route]['params'];
                    $this->routes[$method][$route]['params'] = [];

                    foreach ($params as $key => $param) {
                        $this->routes[$method][$route]['params'][str_replace(array("[:", "]"), "", $routeParams[$key])] = $param;
                    }

                    return $this->routes[$method][$route];
                }

            }

            if ($method == "console")
                return ['url'=>'', 'action' => '\EasyEngine\Classes\Console@index', 'params' => []];

            return ['url' => 404, 'action' => 404, 'params' => []];
        }
        return "";
    }

    /**
     * @param $url
     * @param $action
     * @param $method
     */
    private function setRoute($url, $action, $method)
    {

        preg_match("/\\[:.*\\]/", $url, $params);

        if (count($params) > 0)
            foreach ($params as $param)
                $url = str_replace($param, "(.*)", $url);

        $this->routes[$method][$this->actualGroup . $url] = array('url' => $this->actualGroup . $url, 'action' => $action, 'params' => $params);
    }

    public function setNameSpace($namespace)
    {
        $this->namespaceFiles = $namespace;
    }

    public function getNameSpace()
    {
        return $this->namespaceFiles;
    }

    /**
     * Singleton
     */

    private static $instancia;

    public static function getInstance()
    {
        if (!self::$instancia instanceof self) {
            self::$instancia = new self;
        }
        return self::$instancia;
    }

    public function __clone()
    {
        trigger_error("Operacion Invalida: No puedes clonar una instancia de " . get_class($this) . " class.", E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error("No puedes deserializar una instancia de " . get_class($this) . " class.");
    }

}
