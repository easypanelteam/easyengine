<?php


namespace EasyEngine\Classes;
use Illuminate\Database\Eloquent\Model as ElModel;

abstract class Model extends ElModel
{

    /**
     * @var App
     */
    protected $app;

    /**
     * @var Cache
     */
    protected $cache;

    public function __construct($class = "")
    {

        $this->app = App::getInstance();
        
        if ($this->app->getCfg("USE_BD") == false)
            return false;

        
        parent::__construct();

        $this->cache = $this->app->cache;
    }

    public function setEmptyModeSql()
    {
        return App::getInstance()->db->raw("SET sql_mode='';");
    }

    public function getLastQueries()
    {
        return App::getInstance()->db->getQueryLog();
    }

}