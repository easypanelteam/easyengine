<?php

/**
 * Archivo para inclusion de páginas requeridas
 *
 * @package EasyEnginePHP
 * @author Federicorp <federicorp@easypanelphp.com>
 * @copyright 2016
 *
 */

namespace EasyEngine\Classes;

class Page
{

    //Librerias a utilizar

    /**
     * @var app|null
     */
    private $app = null;

    /**
     * @var permissions|null
     */
    private $permisos = null;

    public $libraries = null;

    private $paginaActual = "";

    private $isAPI = false;


    public function __construct()
    {
        $this->app = App::getInstance();

        $this->libraries = new PageLibraries();

        $this->permisos = Permissions::getInstance();
    }


    /**
     * Se procesan las peticiones.
     *
     * @return void
     */
    public function dispatcher()
    {
        //Se obtiene una instancia del router
        $route = Router::getInstance();

        $_url = $route->getFullUrl();

        //Si se actualiza el sistema
        /*if (strtolower($route->getController()) == "sys__easyupdate") {
            $updater = new Updater();
            $updater->update();
        }*/
        

        //$this->setPage();

        $datosPage = FALSE;

        if ($this->app->getCfg("USE_PERMISSIONS") !== null && $this->app->getCfg("USE_PERMISSIONS") && $this->app->user->isLogged()) {
            // Consulta a la base de datos para obtener datos de la pagina
            $resultSet = $this->app->db->table("pages")->select("permId", "pagTitle", "pagDescription")->where("pagPath", $_url)->get()->all();

            if (count($resultSet) > 0) {
                $row = $resultSet[0];
                $datosPage['permiso'] = $row->permId;

                $this->view->setVar("tituloPagina", $row->pagTitle);
                $this->view->setVar("descripcionPagina", utf8_encode($row->pagDescription));

                //Se comprueban los permisos
                $permisos = $row->permId;
                if (!$this->permisos->checkIf($permisos)) {
                    $this->noPerm();
                    //exit("No tiene permisos para ver la pagina");
                }

            }
        }

        //Se ejecuta(n) el(los) controlador(es)
        $result = $this->controllerExec();

        if ($result == null)
            $result = $this->viewFromController();
        
        //Se renderizan(n) la(s) vista(s)
        if (is_object($result) && is_a($result, View::class))
            $result->renderIt();
        else        
            echo $result;
    }

    //Usuario no tiene permisos para ver la página
    private function noPerm()
    {
        //die("No tiene permisos para ver la página");
        $this->bgRedirect("/403");
    }

    /**
     * Ejecucion de controllers
     *
     * @return void
     * @author Federico Romero
     * @since 15-02-2016
     */
    private function controllerExec()
    {
        
        $route = Router::getInstance();

        $controller = $route->getFullController();
        $action = $route->getAction();

        $request = new Request();
        $request->setParam($route->getAllParams());
        
        $result = null;

        if (class_exists($controller)) {
            $ctrl = new $controller();

            //Se ejecuta el Before Start Trigger
            $faction = "__beforeStart";
            if (method_exists($ctrl, $faction))
                $ctrl->$faction();

            if (method_exists($ctrl, $action))
                $result = $ctrl->$action($request);

            //Se ejecuta el After Finish Trigger
            $lAction = "__afterFinish";
            if (is_object($ctrl) && method_exists($ctrl, $lAction))
                $ctrl->$lAction();
        }
        
        return $result;


    }

    private function viewFromController()
    {
        //Se obtiene la instancia del router
        $route = Router::getInstance();
        $controller = $route->getController();
        $action = $route->getAction();

        $templates = array(
            $controller . "/" . $action,
            $controller . "_" . $action,
            $controller . "/index",
            $controller,
            $action
        );

        return new View($templates);
    }

    public function bgRedirect($url)
    {

        $route = Router::getInstance();

        $_GET['_url'] = $url;

        $route->dispatch();
        exit();
    }

    public function controllerIs($controller = "index")
    {
        $route = Router::getInstance();

        if ($route->getController() == $controller)
            return true;
        return false;
    }

    public function setPage($pagina = "")
    {
            $this->paginaActual = $pagina;
    }

    /**
     * Hace una redireccion a una url o a una pagina dentro del panel
     *
     * @param $url
     * @param bool $isPage
     */
    public function redirectTo($url, $isPage = false)
    {
        if (headers_sent()) {
            //Si las cabeceras ya han sido enviadas
            echo '<meta http-equiv="refresh" content="0;URL=\'' . ABS_URL . $url . '\'" />';
        } else {
            //Si las cabeceras aún no han sido enviadas
            header("Location: " . ABS_URL . $url);
        }
    }
    
    public function setViewDirectory($dir)
    {
        $this->view = new View($dir);
    }

    /**
     * Singleton
     */
    private static $instancia;

    public static function getInstance()
    {
        if (!self::$instancia instanceof self) {
            self::$instancia = new self();
        }
        return self::$instancia;
    }

    public function __clone()
    {
        trigger_error("Operación Invalida: No puedes clonar una instancia de " . get_class($this) . " class.", E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error("No puedes deserializar una instancia de " . get_class($this) . " class.");
    }
}
