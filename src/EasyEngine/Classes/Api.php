<?php

/**
 * Clase de apis
 *
 * @package EasyEnginePHP
 * @author Federico Romero <federicorp@easypanelphp.com>
 * @copyright 2016
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

namespace EasyEngine\Classes;

class Api extends Controller
{
    public $headerData;

    protected $response;

    /**
     * Constructor de la clase Base Api Controller
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this -> response = array('status' => '', 'statusText' => '');

    }

    /**
     * Manejo de Datos
     */

    public function getDataFromHeaders()
    {
        $data = FALSE;

        $this -> headerData = file_get_contents('php://input');

        $dataDecoded = json_decode($this -> headerData, true);

        if (!is_null($dataDecoded)) {
            $data = $dataDecoded;
        }

        return $data;
    }

    /**
     * Respuesta del API
     */

    public function setErrorResponse($statusText = '', $data = array())
    {
        $this -> response['status'] = 'ERROR';
        $this -> response['statusText'] = $statusText;
        $this -> response['data'] = is_array($data) || is_object($data) ? $data : array($data);
    }

    public function setSuccessResponse($statusText = '', $data = array())
    {
        $this -> response['status'] = 'OK';
        $this -> response['statusText'] = $statusText;
        $this -> response['data'] = is_array($data) || is_object($data) ? $data : array($data);
    }

    public function showResponse()
    {
        header('Content-Type: application/json');
        $result = json_encode($this -> response);
        if ($result) {
            echo $result;
        } else {
            $this->setErrorResponse(json_last_error_msg());
            $this->showResponse();
        }
        exit();
    }

    /**
     * Triggers
     */
    public function __beforeStart()
    {

    }

    public function __afterFinish()
    {
        $this->showResponse();
    }
}