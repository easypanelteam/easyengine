<?php

/**
 * Class tracker para logs de eventos del sistema
 * @package EasyEnginePHP
 * @author Federicorp <federicorp@easypanelphp.com>
 * @copyright 2016 Federicorp
 */

namespace EasyEngine\Classes;

use EasyEngine\Models\Tracker as Model;
use EasyEngine\Models\TrackerLevels as TrackLvlModel;

class Tracker
{

    /**
     * @var App
     */
    protected $app;

    /**
     * @var Users|null
     */
    private $user = null;

    /**
     * @var array $logsLevels Se guardan los niveles de logueos que se usaran.
     */
    private $logsLevels = array();

    /**
     * @var array $logsLevels Se usa como cache de consulta SQL
     */
    private $allLogsLevels = array();

    /**
     * @var \Illuminate\Database\Capsule\Manager
     */
    private $sql;

    /**
     * @var bool
     */
    private $active = false;

    /**
     * Tracker constructor.
     */
    public function __construct()
    {

        $this->app = App::getInstance();


        //Si en la configuracion esta activo el tracking
        $this->active = $this->app->getCfg("TRACKING") != NULL ? $this->app->getCfg("TRACKING") : $this->active;

        if ($this->active) {

            //Se obtiene la libreria de usuarios
            $this->user = Users::getInstance();

            //Se obtienen los niveles de logs
            $this->getLogsLevels();

            $this->sql = $this->app->db;
        }



    }


    /**
     * Funcion para registrar en el tracker los logs
     *
     * @param $tipo
     * @param $evento
     * @param $moreData
     * @return bool
     * @throws \Exception
     *
     */
    public function register($tipo, $evento, $moreData = '')
    {

        //Si no se registra nada por configuracion
        if (!$this->active)
            return true;

        //Obtiene id de tipo de log
        $tpLog = $this->getLogLevel($tipo);

        //Si esta dentro de los niveles de logueo actuales del sistema
        if (in_array($tpLog, $this->logsLevels)) {

            //Se recupera la IP del usuario
            $ip = $this->app->lib->getRemoteIP();

            $userId = $this->app->user->getId();

            if ($userId == null) $userId = 0;


            try {
                $this->sql->table("tracker")->insert(
                    ["trcIp" => $ip, "trcAccion" => $evento, "trcDescription" => $moreData, "trctpacId" => $tipo, "userId" => $userId]
                );
                return true;
            } catch (\Exception $e) {
                throw (new \Exception("No se pudo loguear la accion en la BD. Error: " . $e));
                return false;
            }
        }
        return true;

    }


    /******** Modelos de Datos **********/

    /**
     * Funcion para obtener tipo de logs a ser incluidos y guardados
     *
     * @return void
     */
    function getLogsLevels()
    {

        $sql = $this->app->db;

        //Se obtiene el nivel de tracking de la configuracion del sistema
        $result = $this->app->getSysOption("trackingLevel");

        $trackingLevel = 1;
        if ($result !== false)
            $trackingLevel = $result;

        if (!empty($trackingLevel) && is_numeric($trackingLevel)) {

            $sqlResult = TrackLvlModel::select("trclvlId")->where("trclvlNumber", ">=", $trackingLevel)->get()->all();

            foreach ($sqlResult as $value) {
                $this->logsLevels[] = $value->trclvlId;
            }

        } else {
            die("El nivel de tracking no puede ser vacio ni string");
        }
    }

    /**
     * Funcion para obtener el nivel de log
     *
     * @param $tipo
     * @return string
     */
    private function getLogLevel($tipo)
    {

        if (!isset($this->allLogsLevels[$tipo])) {

            $sqlResult = $this->sql->table("tracker_type_actions")->select("trclvlId")->where("trctpacId", $tipo)->get()->all();

            if (count($sqlResult) > 0) {
                return $this->allLogsLevels[$tipo] = $sqlResult[0]->trclvlId;
            }

            //Return por default
            return "ALL";

        } else {
            return $this->allLogsLevels[$tipo];
        }

    }


    /**
     * Singleton
     */
    private static $instancia;

    /**
     * @return Tracker
     */
    public static function getInstance()
    {
        if (!self::$instancia instanceof self) {
            self::$instancia = new self;
        }
        return self::$instancia;
    }

    public function __clone()
    {
        trigger_error("Operacion Invalida: No puedes clonar una instancia de " . get_class($this) . " class.", E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error("No puedes deserializar una instancia de " . get_class($this) . " class.");
    }
}