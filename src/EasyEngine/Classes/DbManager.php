<?php

/**
 * Clase de Drivers de BD
 *
 * @package EasyEnginePHP
 * @author Federicorp <federicorp@easypanelphp.com>
 * @copyright 2014
 *
 */

namespace EasyEngine\Classes;

class DbManager
{

    /**
     * @var \Illuminate\Database\Capsule\Manager
     */
    private $capsules;

    /**
     * @var array of indexes for conections
     */
    private $indexes = array();

    public function __construct()
    {

    }

    /**
     * Setea las configuraciones de las BD
     *
     * @return void
     * @author Federicorp <federicorp@easypanelphp.com>
     */
    public function setConf($config)
    {
        if ($config['USE_BD']) {

            $this->capsules = new \Illuminate\Database\Capsule\Manager();


            $defaultSetted = false;

            //Para cada configuracion de BD
            foreach (array_reverse($config['BD'], true) as $key => $value) {

                if (count($value) > 0) {

                    $name = $key;

                    if (isset($value['DEFAULT']) || ($key == 0 && !$defaultSetted)) {
                        $defaultSetted = true;
                        $name = "default";
                    }

                    $this->capsules->addConnection([
                        'driver' => isset($value['TYPE']) ? strtolower($value['TYPE']) : "mysql",
                        'host' => isset($value['HOST']) ? $value['HOST'] : "localhost",
                        'database' => isset($value['NAMEDB']) ? $value['NAMEDB'] : "",
                        'username' => isset($value['USER']) ? $value['USER'] : "root",
                        'password' => isset($value['PASS']) ? $value['PASS'] : "",
                        'charset' => 'utf8',
                        'collation' => 'utf8_unicode_ci',
                        'prefix' => '',
                    ],
                        $name);

                    $this->indexes[] = $name;

                }

            }

            $this->capsules->bootEloquent();

        }

    }

    /**
     * Metodo para obtener la(s) conexiones
     *
     * @param string - Cual de las conexiones retornar, vacio para la default
     * @return bool|\Illuminate\Database\Connection
     * @author Federicorp <federicorp@easypanelphp.com>
     */
    public function getConn($name = "default")
    {

        $result = FALSE;

        if (count($this->indexes) > 0) { //Si hay
            if ($name == "ALL") {
                foreach ($this->indexes as $value) {
                    $result[$value] = $this->capsules->getConnection($value);
                }
            } else {

                if (in_array($name, $this->indexes))
                    $result = $this->capsules->getConnection($name);

            }
        }


        return $result;

    }

    /**
     * Metodo para agregar conexiones a las anteriores.
     *
     * @param $conf array
     * @param $name string
     * @return void
     * @throws \Exception
     */
    public function addConn($conf, $name)
    {

        if (empty($name))
            throw new \Exception("Nombre de conexion no puede ser vacio.");

        $this->capsules->addConnection([
            'driver' => isset($conf['TYPE']) ? strtolower($conf['TYPE']) : "mysql",
            'host' => isset($conf['HOST']) ? $conf['HOST'] : "localhost",
            'database' => isset($conf['NAMEDB']) ? $conf['NAMEDB'] : "",
            'username' => isset($conf['USER']) ? $conf['USER'] : "root",
            'password' => isset($conf['PASS']) ? $conf['PASS'] : "",
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ],
            $name);

        $this->indexes[] = $name;
    }


    /**
     * Metodo para comprobar si la conexion se realizo o esta activa
     *
     * @param $name string
     * @return bool
     * @throws \Exception
     */
    public function isConnected($name)
    {

        try {
            $this->getConn($name)->getPdo();
        }catch (\PDOException $e) {
            //var_dump($e->getMessage()); exit;
            return false;
        }

        return true;
    }

}

