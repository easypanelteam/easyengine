<?php

namespace EasyEngine\Classes;

class VarsHandle
{

    /**
     * Array de variables
     *
     * @var array
     */
    private $variables = array();

    /**
     * VarsHandle constructor.
     */
    public function __construct()
    {

    }

    /**
     * Setter
     *
     * @param $name
     * @param $value
     */
    public function set($name, $value)
    {
        $this->variables[$name] = $value;
    }

    /**
     * Getter
     *
     * @param $name
     * @return mixed
     */
    public function get($name)
    {
        return $this->variables[$name];
    }

    /**
     * Exists
     *
     * @param $name
     * @return bool
     */
    public function exists($name)
    {
        if (isset($this->variables[$name])) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Gets all values on class
     *
     * @return array
     */
    public function getAll()
    {
        return $this->variables;
    }

}
